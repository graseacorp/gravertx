/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.grasea.gravertx.module;

import com.grasea.gravertx.GravertxServer;
import com.grasea.gravertx.Table;
import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.auth.User;
import io.vertx.ext.sql.SQLConnection;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author USER
 */
public class AccountManager extends BaseManager {

    public static AccountManager instance;
    public static final String EVENT_LOGIN = "EVENT_LOGIN";//param: User object

    public AccountManager(GravertxServer server) {
        super(server);
        if (instance == null) {
            instance = this;
        }
    }

    public static AccountManager get() {
        return instance;
    }

    public void registerRouter(Router router) {
        server.enableRouteAuthority(router, "/api/*");

        router.put("/createDefaultAdmin").handler(this::createDefaultAdmin);
        router.post("/login").handler(this::login);
        router.get("/logout").handler(this::logout);
        router.get("/api/users").handler(this::listUsers);
        router.post("/register").handler(this::register);
        router.post("/test_session").handler(rc -> {
            if (rc.user() == null) {
                responser.ng(rc, "No login yet");
            } else {
                JsonObject session = new JsonObject();
                for (String key : rc.session().data().keySet()) {
                    session.put(key, rc.session().get(key).toString());
                }
                responser.ok(rc, session);
            }
        });
    }

    public void login(RoutingContext routingContext) {
        JsonObject jo = new JsonObject();
        jo.put("username", routingContext.request().getParam("account"));
        jo.put("password", routingContext.request().getParam("password"));
        server.getAuthProvider().authenticate(jo, handler -> {
            if (handler.succeeded()) {
                routingContext.setUser(handler.result());
                JsonObject user = new JsonObject();
                user.put("account", routingContext.request().getParam("account"));
                for (String key : routingContext.request().params().names()) {
                    if (!"account".equals(key) && !"password".equals(key) && !"redirect".equals(key)) {
                        routingContext.session().put(key, routingContext.request().params().get(key));
                        user.put(key, routingContext.request().params().get(key));
                    }
                }
                server.getVertx().eventBus().publish(EVENT_LOGIN, user);
                if (routingContext.request().getParam("redirect") != null) {
                    responser.redirect(routingContext, routingContext.request().getParam("redirect"));
                } else {
                    responser.ok(routingContext, "login success");
                }
            } else {
                responser.ng(routingContext, handler.cause());
            }
        });
    }

    public void updateUser(RoutingContext rc, String setFields, Handler<AsyncResult<JsonObject>> h) {
        if (rc.user() != null && rc.user().principal().containsKey("username")) {
            String account = rc.user().principal().getString("username", "");
            updateAndQuery("Update " + Table.ACCOUNT + " set " + setFields + " where account='" + account + "'", "Select * from " + Table.ACCOUNT + " where account='" + account + "'", res -> {
                if (res.succeeded()) {
                    if (res.result().isEmpty()) {
                        h.handle(Future.failedFuture("No this user"));
                    } else {
                        h.handle(Future.succeededFuture(filter(res.result().get(0))));
                    }
                } else {
                    h.handle(Future.failedFuture(res.cause()));
                }
            });
        } else {
            h.handle(Future.failedFuture("User doesn't login"));
        }
    }

    /**
     *
     * @param routingContext could be null, session won't be set.
     * @param username
     * @param password
     * @param h
     */
    public void login(RoutingContext routingContext, String username, String password, Handler<AsyncResult<JsonObject>> h) {
        JsonObject jo = new JsonObject();
        jo.put("username", username);
        jo.put("password", password);
        server.getAuthProvider().authenticate(jo, handler -> {
            if (handler.succeeded()) {
                if (routingContext != null) {
                    routingContext.setUser(handler.result());
                    connectDB(res -> {
                        if (res.succeeded()) {
                            res.result().query("select * from " + Table.ACCOUNT + " where account='" + username + "'", result -> {
                                if (result.succeeded()) {
                                    if (result.result().getRows().isEmpty()) {
                                        h.handle(Future.failedFuture("User doesn't exist"));
                                    } else {
                                        JsonObject user = filter(result.result().getRows().get(0));
                                        h.handle(Future.succeededFuture(user));
                                    }
                                } else {
                                    h.handle(Future.failedFuture(result.cause()));
                                }
                            });
                        } else {
                            h.handle(Future.failedFuture(res.cause()));
                        }
                    });
                } else {
                    h.handle(Future.succeededFuture(handler.result().principal()));
                }
            } else {
                h.handle(Future.failedFuture(handler.cause()));
            }
        });
    }

    public void logout(RoutingContext routingContext) {
        routingContext.clearUser();
        routingContext.session().destroy();
        if (routingContext.request().getParam("redirect") != null) {
            responser.redirect(routingContext, routingContext.request().getParam("redirect"));
        } else {
            responser.ok(routingContext, "logout success");
        }
    }

    public void register(RoutingContext routingContext) {
    }

    public void register(String username, String password, Handler<AsyncResult<Boolean>> h) {
        if (username == null || password == null) {
            h.handle(Future.failedFuture("parameter username or password is missing"));
            return;
        }
        connectDB(conRes -> {
            if (conRes.succeeded()) {
                SQLConnection connection = conRes.result();
                //System.out.println("execute: insert into account ('admin','" + getSHA512SecurePassword(password, salt) + "','" + salt + "');");
                String insertSQL = "insert into " + Table.ACCOUNT + " (account) values ('" + username + "');";
                String querySQL = "select * from " + Table.ACCOUNT + " where account='" + username + "';";

                updateAndQuery(insertSQL, querySQL, res -> {
                    if (res.succeeded() && res.result().size() > 0) {
                        connectDB(res2 -> {
                            if (res2.succeeded()) {
                                JsonObject inserted = res.result().get(0);
                                int id = inserted.getInteger("id");
                                String salt = inserted.getString("salt");
                                ArrayList<String> sqls = new ArrayList<>();
                                //sqls.add("insert into " + Table.ACCOUNT_ROLES + " (account,role) values ('" + username + "','admin');");
                                //sqls.add("insert into " + Table.ACCOUNT_ROLES + " (account,role) values ('" + username + "','dba');");
                                sqls.add("update " + Table.ACCOUNT + " set password='" + getSHA512SecurePassword(password, salt) + "' where id=" + id + ";");
                                res2.result().batch(sqls, ur -> {
                                    if (ur.succeeded()) {
                                        h.handle(Future.succeededFuture(true));
                                    } else {
                                        h.handle(Future.failedFuture(ur.cause()));
                                    }
                                });
                            } else {
                                h.handle(Future.failedFuture(res2.cause()));
                            }
                        });
                    } else {
                        h.handle(Future.failedFuture(res.cause()));
                    }
                });
            } else {
                h.handle(Future.failedFuture(conRes.cause()));
            }
        });
    }

    public void changePassword(String username, String password, Handler<AsyncResult<Boolean>> h) {
        if (username == null || password == null) {
            h.handle(Future.failedFuture("parameter username or password is missing"));
            return;
        }
        connectDB(conRes -> {
            if (conRes.succeeded()) {
                SQLConnection connection = conRes.result();
                String querySQL = "select * from " + Table.ACCOUNT + " where account='" + username + "';";
                connection.query(querySQL, res -> {
                    if (res.succeeded() && res.result().getRows().size() > 0) {
                        connectDB(res2 -> {
                            if (res2.succeeded()) {
                                JsonObject user = res.result().getRows().get(0);
                                int id = user.getInteger("id");
                                String salt = user.getString("salt");
                                res2.result().update("update " + Table.ACCOUNT + " set password='" + getSHA512SecurePassword(password, salt) + "' where id=" + id + ";", ur -> {
                                    if (ur.succeeded()) {
                                        if (ur.result().getUpdated() == 1) {
                                            h.handle(Future.succeededFuture(true));
                                        } else {
                                            h.handle(Future.failedFuture("Fail to change password of user " + username));
                                        }
                                    } else {
                                        h.handle(Future.failedFuture(ur.cause()));
                                    }
                                });
                            } else {
                                h.handle(Future.failedFuture(res2.cause()));
                            }
                        });
                    } else {
                        h.handle(Future.failedFuture(res.cause()));
                    }
                });
            } else {
                h.handle(Future.failedFuture(conRes.cause()));
            }
        });
    }

    public void listUsers(RoutingContext routingContext) {
        connectDB(routingContext, (connection) -> {
            connection.query("select * from " + Table.ACCOUNT, rs -> {
                if (rs.succeeded()) {
                    JsonArray arr = new JsonArray();
                    rs.result().getRows().forEach(v -> arr.add(filter(v)));
                    responser.ok(routingContext, new JsonObject().put("accounts", arr));
                } else {
                    responser.ng(routingContext, rs.cause());
                }
            });
        });
    }

    public void createDefaultAdmin(RoutingContext routingContext) {
        connectDB(routingContext, (connection) -> {
            connection.query("select * from " + Table.ACCOUNT, rs -> {
                if (rs.succeeded()) {
                    if (rs.result().getNumRows() == 0) {
                        String account = routingContext.request().getParam("account");
                        String password = routingContext.request().getParam("password");
                        if (account == null || password == null) {
                            responser.ng(routingContext, "parameter is missing");
                            return;
                        }
                        //System.out.println("execute: insert into account ('admin','" + getSHA512SecurePassword(password, salt) + "','" + salt + "');");
                        String insertSQL = "insert into " + Table.ACCOUNT + " (account) values ('" + account + "');";
                        String querySQL = "select * from " + Table.ACCOUNT + " where account='" + account + "';";

                        updateAndQuery(insertSQL, querySQL, res -> {
                            if (res.succeeded() && res.result().size() > 0) {
                                connectDB(res2 -> {
                                    if (res2.succeeded()) {
                                        JsonObject inserted = res.result().get(0);
                                        int id = inserted.getInteger("id");
                                        String salt = inserted.getString("salt");
                                        ArrayList<String> sqls = new ArrayList<>();
                                        sqls.add("insert into " + Table.ACCOUNT_ROLES + " (account,role) values ('" + account + "','admin');");
                                        sqls.add("insert into " + Table.ACCOUNT_ROLES + " (account,role) values ('" + account + "','dba');");
                                        sqls.add("update " + Table.ACCOUNT + " set password='" + getSHA512SecurePassword(password, salt) + "' where id=" + id + ";");
                                        res2.result().batch(sqls, ur -> {
                                            if (ur.succeeded()) {
                                                responser.ok(routingContext);
                                            } else {
                                                responser.ng(routingContext, "database operation fail", ur.cause());
                                            }
                                        });
                                    } else {
                                        responser.ng(routingContext, "fail to create admin");
                                    }
                                });
                            } else {
                                responser.ng(routingContext, "fail to create admin");
                            }
                        });
                    } else {
                        responser.ng(routingContext, "not allow to create admin");
                    }
                } else {
                    responser.ng(routingContext, "table may not exist", rs.cause());
                }
            });
        });
    }

    public void addFakeUsers(RoutingContext routingContext) {
        User user = routingContext.user();
        if (user == null) {
            responser.ng(routingContext, "Unauthorized");
        } else {
            final int count = Integer.parseInt(routingContext.request().getParam("count"));
            connectDB(routingContext, (connection) -> {
                ArrayList<String> sqls = new ArrayList<>();

                for (int i = 0; i < count; i++) {
                    sqls.add("insert into " + Table.ACCOUNT + " (account) values ('rovers" + (i + 1) + "@grasea.com');");
                }
                System.out.println("try to insert " + count + " recoreds...");
                connection.batch(sqls, ar -> {
                    if (ar.succeeded()) {
                        System.out.println("success to insert");
                        routingContext.response().putHeader("content-type", "application/json").end(new JsonObject().put("result", 1).encode());
                        responser.ok(routingContext);
                    } else {
                        System.out.println("fail to insert");
                        responser.ng(routingContext, "fail to batch insert account", ar.cause());
                    }
                });
            });
        }
    }

    public String getSHA512SecurePassword(String passwordToHash, String salt) {
        String generatedPassword = null;
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-512");
            md.update(salt.getBytes("UTF-8"));
            byte[] bytes = md.digest(passwordToHash.getBytes("UTF-8"));
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < bytes.length; i++) {
                sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
            }
            generatedPassword = sb.toString().toUpperCase();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException ex) {
            ex.printStackTrace();
        }
        return generatedPassword;
    }

    private void filter(List<JsonObject> users) {
        users.forEach(user -> {
            user.remove("salt");
        });
    }

    private JsonObject filter(JsonObject user) {
        user.remove("salt");
        return user;
    }
}
