/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.grasea.gravertx.module;

import com.grasea.gravertx.GravertxServer;
import com.grasea.gravertx.scheduler.TimerCondition;
import com.grasea.gravertx.scheduler.TimerObject;
import com.grasea.gravertx.scheduler.TimerizeCollection;
import com.grasea.gravertx.util.TimeUtil;
import io.vertx.core.Handler;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Router;
import java.util.concurrent.ConcurrentHashMap;

/**
 *
 * @author USER
 */
public class TimerManager extends BaseManager {

    protected static TimerManager instance;
    protected ConcurrentHashMap<String, TimerizeCollection> timerCollectionMap;
    protected ConcurrentHashMap<String, TimerObject> timerObjectMap;

    public TimerManager(GravertxServer server) {
        super(server);
        timerCollectionMap = new ConcurrentHashMap<>();
        timerObjectMap = new ConcurrentHashMap<>();
        instance = this;
    }

    public static TimerManager get() {
        return instance;
    }

    @Override
    public void registerRouter(Router router) {
    }

    public void timerizeCollection(String jobName, String collection, String keyField, String timeField, TimerCondition condition, Handler<JsonObject> h) {
        TimerizeCollection col = new TimerizeCollection(jobName, collection, keyField, timeField, condition, h);
        switch (condition.getPolicy()) {
            case All:
                timerCollectionMap.put(jobName, col);
                col.initializeMatchedObjects(server.getMongoClient(), timerObjectMap);
                break;
        }
    }

    public TimerizeCollection getTimerizeCollection(String jobName) {
        return timerCollectionMap.get(jobName);
    }

    public void cancelTimerTask(String jobName, JsonObject obj) {
        TimerizeCollection col = timerCollectionMap.get(jobName);
        if (col != null) {
            String taskKey = col.getObjectKey(obj);
            if (timerObjectMap.containsKey(taskKey)) {
                long taskId = timerObjectMap.get(taskKey).getTaskId();
                server.getVertx().cancelTimer(taskId);
            }
        }
    }

    public void rescheduleTimerTask(String jobName, JsonObject obj) {
        TimerizeCollection col = timerCollectionMap.get(jobName);
        if (col != null) {
            String taskKey = col.getObjectKey(obj);
            long newRunTime = obj.getLong(col.getTimeField());
            if (timerObjectMap.containsKey(taskKey)) {
                TimerObject to = timerObjectMap.get(taskKey);
                long taskId = to.getTaskId();
                if (newRunTime != to.getFiredTime()) {
                    server.getVertx().cancelTimer(taskId);
                    if (newRunTime <= TimeUtil.getNow()) {
                        //do nothing
                    } else {
                        col.setupObject(obj, timerObjectMap);
                    }
                } else {
                    //same fire time, do nothing
                }
            } else {
                if (newRunTime > TimeUtil.getNow()) {
                    col.setupObject(obj, timerObjectMap);
                }
            }
        }
    }

    public void done(String taskKey) {
        if (timerObjectMap.containsKey(taskKey)) {
            timerObjectMap.remove(taskKey);
        }
    }
}
