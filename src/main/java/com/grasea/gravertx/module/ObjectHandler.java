/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.grasea.gravertx.module;

import io.vertx.core.AsyncResult;
import io.vertx.core.Handler;

/**
 *
 * @author USER
 */
public interface ObjectHandler<T, S> {

    public void handle(T object, Handler<AsyncResult<S>> handler);
    
}
