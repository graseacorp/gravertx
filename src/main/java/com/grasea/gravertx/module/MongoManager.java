/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.grasea.gravertx.module;

import com.grasea.gravertx.GravertxServer;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.mongo.UpdateOptions;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;

/**
 *
 * @author USER
 */
public class MongoManager extends BaseManager {

    public MongoManager(GravertxServer server) {
        super(server);
    }

    @Override
    public void registerRouter(Router router) {
        if (server.getAuthProvider() != null) {
            server.enableRouteAuthority(router, "/dba/*", "role:dba");
            router.post("/dba/save/:col").handler(this::save);
            router.post("/dba/delete/:col").handler(this::delete);
            router.post("/dba/update/:col").handler(this::update);
            router.post("/dba/find/:col").handler(this::find);
            router.post("/dba/aggregate/:col").handler(this::aggregate);
        }
    }

    public void update(RoutingContext routingContext) {
        String col = routingContext.request().getParam("col");
        JsonObject obj = routingContext.getBodyAsJson();
        if (col != null && obj != null && !obj.isEmpty()) {
            server.getMongoClient().updateCollectionWithOptions(col, new JsonObject(), obj, new UpdateOptions().setMulti(true), result -> {
                if (result.succeeded()) {
                    responser.ok(routingContext);
                } else {
                    responser.ng(routingContext, "fail to update data to " + col);
                }
            });
        } else {
            responser.ng(routingContext, "Missing col attribute or json body");
        }
    }

    public void save(RoutingContext routingContext) {
        String col = routingContext.request().getParam("col");
        JsonObject obj = routingContext.getBodyAsJson();
        if (col != null && obj != null) {
            server.getMongoClient().save(col, obj, result -> {
                if (result.succeeded()) {
                    responser.ng(routingContext, "Object " + obj.getString("_id") + " saved");
                } else {
                    responser.ng(routingContext, "Save object " + obj.getString("_id") + " fail", result.cause());
                }
            });
        } else {
            responser.ng(routingContext, "Missing col attribute or json body");
        }
    }

    public void find(RoutingContext routingContext) {
        String col = routingContext.request().getParam("col");
        JsonObject obj = routingContext.getBodyAsJson();
        if (col != null) {
            server.getMongoClient().find(col, obj == null ? new JsonObject() : obj, result -> {
                if (result.succeeded()) {
                    if (result.result() != null) {
                        responser.ok(routingContext, new JsonObject().put(col, result.result()));
                    } else {
                        responser.ng(routingContext, "Collection " + col + " not exist");
                    }
                } else {
                    responser.ng(routingContext, "Query " + col + " fail", result.cause());
                }
            });
        } else {
            responser.ng(routingContext, "Missing col attribute");
        }
    }

    public void aggregate(RoutingContext routingContext) {
        String col = routingContext.request().getParam("col");
        JsonArray pipeline = routingContext.getBodyAsJsonArray();
        if (col != null) {
            JsonObject command = new JsonObject();
            command.put("aggregate", col).put("pipeline", pipeline);

            server.getMongoClient().runCommand("aggregate", command, result -> {
                if (result.succeeded()) {
                    if (result.result() != null) {
                        responser.ok(routingContext, new JsonObject().put(col, result.result()));
                    } else {
                        responser.ng(routingContext, "Collection " + col + " not exist");
                    }
                } else {
                    responser.ng(routingContext, "Aggregate " + col + " fail", result.cause());
                }
            });
        } else {
            responser.ng(routingContext, "Missing col attribute");
        }
    }

    public void delete(RoutingContext routingContext) {
        String col = routingContext.request().getParam("col");
        JsonObject condition = routingContext.getBodyAsJson();
        if (col != null && condition != null) {
            server.getMongoClient().removeDocuments(col, condition, result -> {
                if (result.succeeded()) {
                    if (result.result() != null) {
                        responser.ok(routingContext, "Delete condition " + condition.toString() + " success");
                    } else {
                        responser.ng(routingContext, "Collection " + col + " not exist");
                    }
                } else {
                    responser.ng(routingContext, "Delete condition " + condition.toString() + " from " + col + " fail", result.cause());
                }
            });
        } else {
            responser.ng(routingContext, "Missing col attribute or body is not a condition obj");
        }
    }
}
