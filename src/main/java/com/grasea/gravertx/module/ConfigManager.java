/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.grasea.gravertx.module;

import com.grasea.gravertx.GravertxServer;
import com.grasea.gravertx.model.CollectionCache;
import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author USER
 */
public class ConfigManager extends BaseManager {

    protected static ConfigManager instance;
    protected static final String CONFIG_COLLECTION = "GravertxConfig";
    protected ConcurrentHashMap<String, Object> configMap;
    protected CollectionCache configTypeCache;

    public ConfigManager(GravertxServer server) {
        super(server);
        configMap = new ConcurrentHashMap<String, Object>();
        configTypeCache = new CollectionCache(server.getMongoClient(), CONFIG_COLLECTION, "type");
        instance = this;
    }

    @Override
    public void registerRouter(Router router) {

        configTypeCache.loadCollectionData(res -> {
            if (res.succeeded()) {
                configTypeCache.iterate(type -> {
                    type.forEach(entry -> {
                        if (entry.getValue() instanceof JsonObject) {
                            configMap.put(((JsonObject) entry.getValue()).getString("key"), ((JsonObject) entry.getValue()).getValue("value"));
                        }
                    });
                });
                ConfigBuilder builder = new ConfigBuilder();
                initConfigMap(builder);
                builder.commitNonexistConfigs();
            }
        });
        router.get("/api/config/:type").handler(this::getConfigType);
    }

    public static boolean updateConfigObject(String type, JsonObject configObject) {
        if (type != null && !configObject.isEmpty()) {
            JsonObject configType = instance.configTypeCache.get(type);
            if (configType == null || !configType.containsKey("_id") || !configType.containsKey("type")) {
                return false;
            }
            configObject.forEach(e -> {
                if (configType.containsKey(e.getKey())) {
                    configType.getJsonObject(e.getKey()).put("value", e.getValue());
                } else {
                    configType.put(e.getKey(), new JsonObject().put("key", e.getKey()).put("type", type).put("value", e.getValue()));
                }
                instance.configMap.put(e.getKey(), e.getValue());
            });
            instance.configTypeCache.update(configType, res -> instance.server.log(res));
            return true;
        } else {
            return false;
        }
    }

    public void updateConfig(JsonObject config, Handler<AsyncResult<Boolean>> h) {
        configMap.put(config.getString("key"), config.getValue("value"));
        if (configTypeCache.containsKey(config.getString("type"))) {
            JsonObject configType = configTypeCache.get(config.getString("type"));
            configType.put(config.getString("key"), config);
            configTypeCache.update(configType, h);
        } else {
            JsonObject configType = new JsonObject();
            configType.put("type", config.getString("type"));
            configType.put(config.getString("key"), config);
            configTypeCache.update(configType, h);
        }
    }

    public static int getInt(String key) {
        return (Integer) instance.configMap.get(key);
    }

    public static String get(String key) {
        return instance.configMap.get(key).toString();
    }

    public static double getDouble(String key) {
        return (Double) instance.configMap.get(key);
    }

    public static boolean getBoolean(String key) {
        return (Boolean) instance.configMap.get(key);
    }

    public static JsonObject getObject(String key) {
        return (JsonObject) instance.configMap.get(key);
    }

    public static JsonArray getArray(String key) {
        return (JsonArray) instance.configMap.get(key);
    }

    public static JsonObject getConfigs(String configType) {
        if (instance.configTypeCache.containsKey(configType)) {
            JsonObject config = new JsonObject();
            instance.configTypeCache.get(configType).forEach(e -> {
                if (e.getValue() instanceof JsonObject) {
                    if (((JsonObject) e.getValue()).containsKey("key")) {
                        config.put(((JsonObject) e.getValue()).getString("key"), ((JsonObject) e.getValue()).getValue("value"));
                    }
                }
            });
            return config;
        } else {
            return new JsonObject();
        }
    }

    //should override this method and call builder.putConfig method to create configurations in mongodb and cahced in the map  while system is initializing.
    public void initConfigMap(ConfigBuilder builder) {
    }

    protected void getConfigType(RoutingContext rc) {
        if (rc.request().getParam("type") != null && rc.request().getParam("type").length() > 0) {
            if (configTypeCache.containsKey(rc.request().getParam("type"))) {
                responser.ok(rc, new JsonObject().put(rc.request().getParam("type"), configTypeCache.get(rc.request().getParam("type"))));
            } else {
                responser.ok(rc, new JsonObject().put(rc.request().getParam("type"), new JsonObject()));
            }
        }
    }

    public class ConfigBuilder {

        ConcurrentHashMap<String, JsonObject> initMap;

        public ConfigBuilder() {
            this.initMap = new ConcurrentHashMap<>();
        }

        public ConfigBuilder put(String type, String key, Object value) {
            JsonObject jo = new JsonObject();
            jo.put("key", key);
            jo.put("type", type);
            jo.put("value", value);
            initMap.put(key, jo);
            return this;
        }

        public void commitNonexistConfigs() {
            instance.serialIterate(initMap.keySet().iterator(), (key, h) -> {
                if (!configMap.containsKey(key)) {
                    updateConfig(initMap.get(key), h);
                } else {
                    h.handle(Future.succeededFuture(false));
                }
            }, (AsyncResult<List<Boolean>> res) -> {
                if (res.failed()) {
                    Logger.getLogger(ConfigBuilder.class.getName()).log(Level.SEVERE, null, res.cause());
                }
            });
        }
    }
}
