/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.grasea.gravertx.module;

import com.grasea.gravertx.GravertxServer;
import com.grasea.gravertx.Responser;
import io.vertx.core.AsyncResult;
import io.vertx.core.CompositeFuture;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.mongo.FindOptions;
import io.vertx.ext.mongo.MongoClient;
import io.vertx.ext.sql.SQLConnection;
import io.vertx.ext.sync.Sync;
import io.vertx.ext.web.FileUpload;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.function.Consumer;

/**
 *
 * @author USER
 */
public abstract class BaseManager {

    protected GravertxServer server;
    protected Responser responser;

    public BaseManager(GravertxServer server) {
        this.responser = server.getResponser();
        this.server = server;
    }

    protected BaseManager(BaseManager parentManager) {
        this.server = parentManager.server;
        this.responser = parentManager.responser;
    }

    public GravertxServer getServer() {
        return server;
    }

    public Responser getResponser() {
        return responser;
    }

    public abstract void registerRouter(Router router);

    public boolean hasParam(RoutingContext rc, String param) {
        if (rc.request().getParam(param) != null && !"".equals(rc.request().getParam(param).trim())) {
            return true;
        } else {
            return false;
        }
    }

    public void hasParam(RoutingContext rc, String param, Handler<String> h) {
        if (rc.request().getParam(param) != null && !"".equals(rc.request().getParam(param).trim())) {
            h.handle(rc.request().getParam(param));
        }
    }

    protected <T> T awaitResult(Consumer<Handler<AsyncResult<T>>> consumer) {
        return Sync.awaitResult(consumer);
    }

    protected <T> T awaitEvent(Consumer<Handler<T>> consumer) {
        return Sync.awaitEvent(consumer);
    }

    public void connectDB(RoutingContext routingContext, Handler<SQLConnection> handler) {
        server.getJDBCClient().getConnection(res -> {
            if (res.succeeded()) {
                try (SQLConnection connection = res.result()) {
                    handler.handle(connection);
                    connection.close();
                }
            } else {
                responser.ng(routingContext, "fail to create db connection", res.cause());
            }
        });
    }

    public void connectDB(Handler<AsyncResult<SQLConnection>> handler) {
        server.getJDBCClient().getConnection(res -> {
            if (res.succeeded()) {
                try (SQLConnection connection = res.result()) {
                    handler.handle(Future.succeededFuture(connection));
                    connection.close();
                }
            } else {
                handler.handle(Future.failedFuture(res.cause()));
            }
        });
    }

    public void updateAndQuery(RoutingContext routingContext, String updateSQL, String querySQL, Handler<List<JsonObject>> handler) {
        updateAndQuery(updateSQL, querySQL, res -> {
            if (res.succeeded()) {
                if (res.result().size() > 0) {
                    handler.handle(res.result());
                } else {
                    responser.ng(routingContext, "fail query returing sql");
                }
            } else {
                responser.ng(routingContext, res.cause());
            }
        });
    }

    public void insertAnyway(String insertSQL, String querySQL, Handler<AsyncResult<List<JsonObject>>> handler) {
        connectDB(res -> {
            if (res.succeeded()) {
                res.result().update(insertSQL, result -> {
                    if (!result.succeeded()) {
                        if (!result.cause().getMessage().contains("duplicate key value")) {
                            handler.handle(Future.failedFuture(result.cause()));
                            return;
                        }
                    }
                    connectDB(res2 -> {
                        if (res2.succeeded()) {
                            res2.result().query(querySQL, result2 -> {
                                if (result2.succeeded()) {
                                    handler.handle(Future.succeededFuture(result2.result().getRows()));
                                } else {
                                    handler.handle(Future.failedFuture(result2.cause()));
                                }
                            });
                        } else {
                            handler.handle(Future.failedFuture(res2.cause()));
                        }
                    });
                });
            } else {
                handler.handle(Future.failedFuture(res.cause()));
            }
        });
    }
//    protected void insertAnyway(String insertSQL, String querySQL, Handler<List<JsonObject>> handler, Handler<Throwable> errorHandler) {
//        connectDB(conn -> {
//            conn.update(insertSQL, result -> {
//                if (!result.succeeded()) {
//                    if (!result.cause().getMessage().contains("duplicate key value")) {
//                        errorHandler.handle(result.cause());
//                        return;
//                    }
//                }
//                connectDB(conn2 -> {
//                    conn2.query(querySQL, result2 -> {
//                        if (result2.succeeded()) {
//                            handler.handle(result2.result().getRows());
//                        } else {
//                            errorHandler.handle(result2.cause());
//                        }
//                    });
//                }, errorHandler);
//            });
//        }, errorHandler);
//    }

    public void updateAndQuery(String updateSQL, String querySQL, Handler<AsyncResult<List<JsonObject>>> handler) {
        connectDB(res -> {
            if (res.succeeded()) {
                res.result().update(updateSQL, result -> {
                    if (result.succeeded()) {
                        if (result.result().getUpdated() == 0) {
                            handler.handle(Future.failedFuture(new Throwable("No data is changed")));
                        } else {
                            connectDB(res2 -> {
                                if (res2.succeeded()) {
                                    res2.result().query(querySQL, result2 -> {
                                        if (result2.succeeded()) {
                                            handler.handle(Future.succeededFuture(result2.result().getRows()));
                                        } else {
                                            handler.handle(Future.failedFuture(result2.cause()));
                                        }
                                    });
                                } else {
                                    handler.handle(Future.failedFuture(res2.cause()));
                                }
                            });
                        }
                    } else {
                        handler.handle(Future.failedFuture(result.cause()));
                    }
                });
            } else {
                handler.handle(Future.failedFuture(res.cause()));
            }
        });
    }

    public void insertIfNotExist(String collectionKey, JsonObject jo, String[] keys, Handler<AsyncResult<Boolean>> h) {
        insertIfNotExist(server.getMongoClient(), collectionKey, jo, keys, h);
    }

    public void insertIfNotExist(MongoClient client, String collectionKey, JsonObject jo, String[] keys, Handler<AsyncResult<Boolean>> h) {
        JsonObject condition = new JsonObject();
        for (String key : keys) {
            if (jo.containsKey(key)) {
                condition.put(key, jo.getValue(key));
            }
        }
        client.findOne(collectionKey, condition, new JsonObject(), res -> {
            if (res.succeeded()) {
                if (res.result() == null) {
                    client.insert(collectionKey, jo, res2 -> {
                        if (res2.succeeded()) {
                            h.handle(Future.succeededFuture(true));
                        } else {
                            h.handle(Future.failedFuture(res2.cause()));
                        }
                    });
                } else {
                    h.handle(Future.succeededFuture(false));
                }
            } else {
                h.handle(Future.failedFuture(res.cause()));
            }
        });
    }

    public void insertOrUpdate(String collectionKey, JsonObject jo, String[] keys, Handler<AsyncResult<Boolean>> h) {
        insertOrUpdate(server.getMongoClient(), collectionKey, jo, keys, h);
    }

    public void insertOrUpdate(MongoClient client, String collectionKey, JsonObject jo, String[] keys, Handler<AsyncResult<Boolean>> h) {
        JsonObject condition = new JsonObject();
        for (String key : keys) {
            if (jo.containsKey(key)) {
                condition.put(key, jo.getValue(key));
            }
        }
        client.findOne(collectionKey, condition, new JsonObject(), res -> {
            if (res.succeeded()) {
                if (res.result() == null) {
                    client.insert(collectionKey, jo, res2 -> {
                        if (res2.succeeded()) {
                            h.handle(Future.succeededFuture(true));
                        } else {
                            h.handle(Future.failedFuture(res2.cause()));
                        }
                    });
                } else {
                    jo.put("_id", res.result().getString("_id"));
                    client.save(collectionKey, jo, res2 -> {
                        if (res2.succeeded()) {
                            h.handle(Future.succeededFuture(true));
                        } else {
                            h.handle(Future.failedFuture(res2.cause()));
                        }
                    });
                }
            } else {
                h.handle(Future.failedFuture(res.cause()));
            }
        });
    }

    public void mongoExist(String collection, JsonObject condition, Handler<JsonObject> existHandler, Handler<Void> nonexistHandler) {
        mongoExist(server.getMongoClient(), collection, condition, existHandler, nonexistHandler);
    }

    public void mongoExist(MongoClient client, String collection, JsonObject condition, Handler<JsonObject> existHandler, Handler<Void> nonexistHandler) {
        client.findOne(collection, condition, new JsonObject(), res -> {
            if (res.succeeded() && res.result() != null) {
                if (existHandler != null) {
                    existHandler.handle(res.result());
                }
            } else {
                if (nonexistHandler != null) {
                    nonexistHandler.handle(null);
                }
            }
        });
    }

    public <T, S> void serialIterate(Iterator<T> i, ObjectHandler<T, S> iteratorHandler, Handler<AsyncResult<List<S>>> finalHandler) {
        serialIterate(i, new ArrayList<>(), iteratorHandler, finalHandler);
    }

    private <T, S> void serialIterate(Iterator<T> i, List<S> results, ObjectHandler<T, S> iteratorHandler, Handler<AsyncResult<List<S>>> finalHandler) {
        if (i.hasNext()) {
            Future<S> f = Future.future();

            iteratorHandler.handle(i.next(), f.completer());
            f.setHandler(res -> {
                if (res.succeeded()) {
                    S passed = res.result();
                    if (passed != null) {
                        results.add(passed);
                    }
                    serialIterate(i, results, iteratorHandler, finalHandler);
                } else {
                    finalHandler.handle(Future.failedFuture(res.cause()));
                }
            });
        } else {
            finalHandler.handle(Future.succeededFuture(results));
        }
    }

    public void mongoIterate(String collection, JsonObject condition, Handler<JsonObject> iterateHandler, Handler<Void> nothingHandler) {
        mongoIterate(collection, condition, null, iterateHandler, nothingHandler, null);
    }

    public void mongoIterate(MongoClient client, String collection, JsonObject condition, Handler<JsonObject> iterateHandler, Handler<Void> nothingHandler) {
        mongoIterate(client, collection, condition, null, iterateHandler, nothingHandler, null);
    }

    public void mongoIterate(String collection, JsonObject condition, FindOptions options, Handler<JsonObject> iterateHandler, Handler<Void> nothingHandler, Handler<List<JsonObject>> afterHandler) {
        mongoIterate(server.getMongoClient(), collection, condition, options, iterateHandler, nothingHandler, afterHandler);
    }

    public void mongoIterate(MongoClient client, String collection, JsonObject condition, FindOptions options, Handler<JsonObject> iterateHandler, Handler<Void> nothingHandler, Handler<List<JsonObject>> afterHandler) {
        if (options == null) {
            client.find(collection, condition, res -> {
                if (res.succeeded() && res.result() != null) {
                    if (iterateHandler != null) {
                        res.result().forEach(jo -> iterateHandler.handle(jo));
                    }
                    if (afterHandler != null) {
                        afterHandler.handle(res.result());
                    }
                } else {
                    if (nothingHandler != null) {
                        nothingHandler.handle(null);
                    }
                }
            });
        } else {
            client.findWithOptions(collection, condition, options, res -> {
                if (res.succeeded() && res.result() != null) {
                    if (iterateHandler != null) {
                        res.result().forEach(jo -> iterateHandler.handle(jo));
                    }
                    if (afterHandler != null) {
                        afterHandler.handle(res.result());
                    }
                } else {
                    if (nothingHandler != null) {
                        nothingHandler.handle(null);
                    }
                }
            });
        }
    }

    public <T, S> void syncJob(ObjectHandler<T, S> sync, Handler<AsyncResult<CompositeFuture>> h, T... array) {
        ArrayList<Future> list = new ArrayList<>();
        for (T param : array) {
            Future f = Future.future();
            list.add(f);
            sync.handle(param, f.completer());
        }
        CompositeFuture.all(list).setHandler(h);
    }

    public void conditionalExecute(boolean condition, Handler<AsyncResult<Boolean>> finalHandler, Handler<Handler<AsyncResult<Boolean>>>... conditionHandlers) {
        if (condition) {
            ArrayList<Future> futures = new ArrayList<>();
            for (Handler<Handler<AsyncResult<Boolean>>> h : conditionHandlers) {
                Future<Boolean> f = Future.future();
                h.handle(f.completer());
                futures.add(f);
            }
            CompositeFuture.all(futures).setHandler(res -> {
                if (res.succeeded()) {
                    finalHandler.handle(Future.succeededFuture(true));
                } else {
                    finalHandler.handle(Future.failedFuture(res.cause()));
                }
            });
        } else {
            finalHandler.handle(Future.succeededFuture(true));
        }
    }

    public void handleAsyncResultAsResponse(RoutingContext rc, JsonObject responseObj, AsyncResult res) {
        if (res.succeeded()) {
            if (responseObj == null) {
                responser.ok(rc);
            } else {
                responser.ok(rc, responseObj);
            }
        } else {
            responser.ng(rc, res.cause());
        }
    }

    public void handleAsyncResultAsLog(AsyncResult res) {
        server.log(res);
    }

    public <T> void handlerWrapper(AsyncResult<T> res, Handler<AsyncResult<Object>> h) {
        if (res.succeeded()) {
            h.handle(Future.succeededFuture(res.result()));
        } else {
            h.handle(Future.failedFuture(res.cause()));
        }
    }

    public boolean hasUploadFile(RoutingContext rc, String name) {
        for (FileUpload file : rc.fileUploads()) {
            if (name.equalsIgnoreCase(file.name())) {
                if (file.size() > 0) {
                    return true;
                }
            }
        }
        return false;
    }

    public FileUpload getUploadFile(RoutingContext rc, String name) {
        for (FileUpload file : rc.fileUploads()) {
            if (name.equalsIgnoreCase(file.name())) {
                return file;
            }
        }
        return null;
    }

    public void replaceImage(JsonObject obj, String attribute, String newImage) {
        if (obj != null) {
            if (obj.containsKey(attribute)) {
                String path = obj.getString(attribute).substring(1);
                if (server.getVertx().fileSystem().existsBlocking(path)) {
                    server.getVertx().fileSystem().deleteBlocking(path);
                }
            }
            obj.put(attribute, "/" + newImage.replace("\\", "/"));
        }
    }

    interface ReturningHandler {

        public void onReturning(JsonObject object, SQLConnection connection);
    }
}
