/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.grasea.gravertx;

import co.paralleluniverse.fibers.Suspendable;
import io.vertx.core.AsyncResult;
import io.vertx.core.DeploymentOptions;
import io.vertx.core.Vertx;
import io.vertx.core.VertxOptions;
import io.vertx.core.http.HttpServerOptions;
import io.vertx.core.json.JsonObject;
import io.vertx.core.net.PemKeyCertOptions;
import io.vertx.ext.auth.jdbc.JDBCAuth;
import io.vertx.ext.jdbc.JDBCClient;
import io.vertx.ext.mongo.MongoClient;
import io.vertx.ext.sync.SyncVerticle;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.handler.BasicAuthHandler;
import io.vertx.ext.web.handler.BodyHandler;
import io.vertx.ext.web.handler.CookieHandler;
import io.vertx.ext.web.handler.ErrorHandler;
import io.vertx.ext.web.handler.SessionHandler;
import io.vertx.ext.web.handler.StaticHandler;
import io.vertx.ext.web.handler.UserSessionHandler;
import io.vertx.ext.web.sstore.LocalSessionStore;
import java.io.File;
import java.io.IOException;
import java.util.function.Consumer;

/**
 *
 * @author Grasea
 */
public abstract class GravertxServer extends SyncVerticle {

    private static final String WEB_JAVA_DIR = "src/main/java/";
    private static int port = 8080;
    private static HttpServerOptions serverOptions;

    protected JDBCClient jdbcClient;
    protected MongoClient mongoClient;
    protected JDBCAuth authProvider;
    protected Responser responser;

    public String getUploadDirectory() {
        return "uploads";
    }

    public String getWebrootDirectory() {
        return "webroot";
    }

    public JDBCClient getJDBCClient() {
        return jdbcClient;
    }

    public MongoClient getMongoClient() {
        return mongoClient;
    }

    public JDBCAuth getAuthProvider() {
        return authProvider;
    }

    public Responser getResponser() {
        return responser;
    }

    public void setResponser(Responser responser) {
        this.responser = responser;
    }

    protected abstract void init(Router router);

    public abstract void log(AsyncResult res);

    protected void setupPostgreSQL(String host, int port, String database, String user, String password) {
        jdbcClient = createPostgreClient(host, port, database, user, password);
    }

    protected void setupPostgreSQL(String database, String user, String password) {
        jdbcClient = createPostgreClient(null, 5432, database, user, password);
    }

    public JDBCClient createPostgreClient(String host, int port, String database, String user, String password) {
        if (host == null) {
            host = "localhost";
        }
        JsonObject postgreSQLConfig = new JsonObject().put("url", "jdbc:postgresql://" + host + ":" + port + "/" + database).put("driver_class", "org.postgresql.Driver");
        if (user != null && password != null) {
            postgreSQLConfig.put("user", user).put("password", password);
        }
        return JDBCClient.createShared(vertx, postgreSQLConfig);
    }

    protected void setupMongoDB(String host, int port, String database) {
        mongoClient = createMongoClient(host, port, database);
    }

    protected void setupMongoDB(String database) {
        mongoClient = createMongoClient(null, 0, database);
    }

    public MongoClient createMongoClient(String host, int port, String database) {
        JsonObject mongodbConfig = new JsonObject().put("db_name", database);
        if (host != null) {
            mongodbConfig.put("host", host);
        }
        if (port > 0) {
            mongodbConfig.put("port", port);
        }
        return MongoClient.createShared(vertx, mongodbConfig);
    }

    public MongoClient createMongoClient(String host, int port, String database, String dataSourceName) {
        JsonObject mongodbConfig = new JsonObject().put("db_name", database);
        if (host != null) {
            mongodbConfig.put("host", host);
        }
        if (port > 0) {
            mongodbConfig.put("port", port);
        }
        return MongoClient.createShared(vertx, mongodbConfig, dataSourceName);
    }

    protected void enableJDBCAuthentication(Router router) {
        authProvider = JDBCAuth.create(getVertx(), jdbcClient);
        authProvider.setAuthenticationQuery("SELECT password, salt FROM " + Table.ACCOUNT + " WHERE account = ?");
        authProvider.setRolesQuery("SELECT role FROM " + Table.ACCOUNT_ROLES + " WHERE account = ?");
        authProvider.setPermissionsQuery("SELECT perm FROM " + Table.ROLES_PERMS + " rp, " + Table.ACCOUNT_ROLES + " ur WHERE ur.account = ? AND ur.role = rp.role");
        router.route().handler(CookieHandler.create());
        router.route().handler(SessionHandler.create(LocalSessionStore.create(vertx)));
        router.route().handler(UserSessionHandler.create(authProvider));
    }

    public void enableRouteAuthority(Router router, String path) {
        enableRouteAuthority(router, path, null);
    }

    public void enableRouteAuthority(Router router, String path, String realm) {
        if (authProvider != null) {
            if (realm == null) {
                router.route(path).handler(BasicAuthHandler.create(authProvider));
            } else {
                router.route(path).handler(BasicAuthHandler.create(authProvider, realm));
            }
        }
    }

    @Override
    @Suspendable
    public void start() throws Exception {
        responser = new Responser();
        Router router = Router.router(vertx);
        router.route().handler(BodyHandler.create().setUploadsDirectory(getUploadDirectory()));
        router.route().failureHandler(ErrorHandler.create(true));
        //router.route().handler(this::init);
        init(router);
        router.route().handler(StaticHandler.create().setWebRoot(getWebrootDirectory()));
        if (serverOptions != null) {
            vertx.createHttpServer(serverOptions).requestHandler(router::accept).listen(port);
        } else {
            vertx.createHttpServer().requestHandler(router::accept).listen(port);
        }
    }

    public static void runClustered(Class clazz) {
        GravertxServer.run(WEB_JAVA_DIR, clazz, new VertxOptions().setClustered(true), null);
    }

    public static void run(Class clazz) {
        GravertxServer.run(WEB_JAVA_DIR, clazz, new VertxOptions().setClustered(false), null);
    }

    public static void run(Class clazz, int port) {
        GravertxServer.port = port;
        GravertxServer.run(WEB_JAVA_DIR, clazz, new VertxOptions().setClustered(false), null);
    }

    public static void run(Class clazz, DeploymentOptions options) {
        GravertxServer.run(WEB_JAVA_DIR, clazz, new VertxOptions().setClustered(false), options);
    }

    public static void run(Class clazz, String keyPath, String certPath, int port) {
        GravertxServer.port = port;
        serverOptions = new HttpServerOptions().setSsl(true)
                .setPemKeyCertOptions(new PemKeyCertOptions().setKeyPath("privatekey.key").setCertPath("certificate.cer"));//.setHost("api.fantasyspin.com").setPort(443)
        GravertxServer.run(clazz);
    }

    public static void run(String exampleDir, Class clazz, VertxOptions options, DeploymentOptions deploymentOptions) {
        System.out.println("try to start server: " + clazz.getSimpleName());
        GravertxServer.run(exampleDir + clazz.getPackage().getName().replace(".", "/"), clazz.getName(), options, deploymentOptions);
    }

    public static void run(String exampleDir, String verticleID, VertxOptions options, DeploymentOptions deploymentOptions) {
        if (options == null) {
            // Default parameter
            options = new VertxOptions().setClustered(false).setMaxEventLoopExecuteTime(10000L);
        }
        // Smart cwd detection

        // Based on the current directory (.) and the desired directory (exampleDir), we try to compute the vertx.cwd
        // directory:
        try {
            File current = new File(".").getCanonicalFile();
            if (exampleDir.startsWith(current.getName()) && !exampleDir.equals(current.getName())) {
                exampleDir = exampleDir.substring(current.getName().length() + 1);
            }
        } catch (IOException e) {
            // Ignore it.
        }
        System.setProperty("vertx.cwd", exampleDir);
        Consumer<Vertx> runner = vertx -> {
            try {
                if (deploymentOptions != null) {
                    vertx.deployVerticle(verticleID, deploymentOptions);
                } else {
                    vertx.deployVerticle(verticleID);
                }
            } catch (Throwable t) {
                t.printStackTrace();
            }
        };
        if (options.isClustered()) {
            Vertx.clusteredVertx(options, res -> {
                if (res.succeeded()) {
                    Vertx vertx = res.result();
                    runner.accept(vertx);
                } else {
                    res.cause().printStackTrace();
                }
            });
        } else {
            Vertx vertx = Vertx.vertx(options);
            runner.accept(vertx);
        }
    }

}
