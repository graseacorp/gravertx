/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.grasea.gravertx.resource;

import io.vertx.core.Vertx;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.json.JsonObject;
import java.io.File;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author USER
 */
public class Str {

    private static JsonObject resource = new JsonObject();
    private static CopyOnWriteArrayList<String> langs = new CopyOnWriteArrayList<>();
    private static ConcurrentHashMap<String, String> langPathMap = new ConcurrentHashMap<>();

    public static void load(Vertx vertx, String lang, File path) {
        System.out.println("try to load " + path.getAbsolutePath() + " into " + lang + " resource");
        Buffer b = vertx.fileSystem().readFileBlocking(path.getAbsolutePath());
        if (b != null) {
            JsonObject langObj = b.toJsonObject();
            resource.put(lang, langObj);
            langs.add(lang);
            langPathMap.put(lang, path.getAbsolutePath());
        } else {
            Logger.getLogger(Str.class.getName()).log(Level.SEVERE, "Couldn't find resource file: " + path);
        }
    }

    public static String get(String key) {
        if (!langs.isEmpty()) {
            return getLang(langs.get(0), key);
        } else {
            return "Something went wrong.";
        }
    }

    public static String apply(String key, String... params) {
        if (!langs.isEmpty()) {
            return applyLang(langs.get(0), key, params);
        } else {
            return "Something went wrong.";
        }
    }

    public static String getLang(String lang, String key) {
        return applyLang(lang, key, new String[]{});
    }

    public static String applyLang(String lang, String key, String... params) {
        JsonObject langObj = null;
        if (resource.containsKey(lang)) {
            langObj = resource.getJsonObject(lang);
            if (!langObj.containsKey(key)) {
                langObj = resource.getJsonObject(langs.get(0));
            }
        } else {
            if (!langs.isEmpty()) {
                langObj = resource.getJsonObject(langs.get(0));
            }
        }
        if (langObj != null) {
            if (params.length == 0) {
                return langObj.getString(key, "Something went wrong.");
            } else {
                return String.format(langObj.getString(key, "Something went wrong."), (Object[]) params);
            }
        } else {
            return "Something went wrong.";
        }
    }

    public static String getAnds(String key, String... params) {
        if (params != null) {
            switch (params.length) {
                case 0:
                    return getLang(key, "");
                case 1:
                    return getLang(key, params[0]);
                case 2:
                    return getLang(key, params[0] + " and " + params[1]);
                default:
                    StringBuilder sb = new StringBuilder();
                    for (int i = 0; i < params.length; i++) {
                        if (i < params.length - 2) {
                            sb.append(params[i] + ", ");
                        } else if (i == params.length - 2) {
                            sb.append(params[i] + " and ");
                        } else {
                            sb.append(params[i]);
                        }
                    }
                    return getLang(key, sb.toString());
            }
        }
        return getLang(key, "");
    }

    public static String getOrs(String key, String... params) {
        if (params != null) {
            switch (params.length) {
                case 0:
                    return getLang(key, "");
                case 1:
                    return getLang(key, params[0]);
                case 2:
                    return getLang(key, params[0] + " or " + params[1]);
                default:
                    StringBuilder sb = new StringBuilder();
                    for (int i = 0; i < params.length; i++) {
                        if (i < params.length - 2) {
                            sb.append(params[i] + ", ");
                        } else if (i == params.length - 2) {
                            sb.append(params[i] + " or ");
                        } else {
                            sb.append(params[i]);
                        }
                    }
                    return getLang(key, sb.toString());
            }
        }
        return getLang(key, "");
    }

    public static void put(Vertx vertx, String lang, String key, String string) {
        JsonObject langObj = null;
        if (resource.containsKey(lang)) {
            langObj = resource.getJsonObject(lang);
        } else {
            langObj = new JsonObject();
            resource.put(lang, langObj);
            langs.add(lang);
        }
        langObj.put(key, string);
        if (langPathMap.containsKey(lang)) {
            Buffer b = Buffer.buffer(langObj.encodePrettily());
            vertx.fileSystem().writeFileBlocking(langPathMap.get(lang), b);
        }
    }

    public static JsonObject getLangObject(String lang) {
        return resource.getJsonObject(lang, new JsonObject());
    }
}
