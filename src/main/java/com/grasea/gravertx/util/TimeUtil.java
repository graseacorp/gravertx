/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.grasea.gravertx.util;

import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.temporal.ChronoField;

/**
 *
 * @author USER
 */
public class TimeUtil {

    public static long getNow() {
        return Instant.now().toEpochMilli();
    }

    public static long getDaysMilliSecond(int days) {
        return 86400000L * days;
    }

    public static long getHoursMilliSecond(int hours) {
        return 3600000L * hours;
    }

    public static long getMinutesMilliSecond(int minutes) {
        return 60000L * minutes;
    }

    public static long getLocalHouDiff(JsonArray timeArray) {
        int hl = -1;
        int hu = -1;
        for (int i = 0; i < timeArray.size(); i++) {
            if (timeArray.getJsonObject(i).getString("dateType").equals("UTC")) {
                hu = timeArray.getJsonObject(i).getInteger("hour");
            } else if (timeArray.getJsonObject(i).getString("dateType").equals("Local")) {
                hl = timeArray.getJsonObject(i).getInteger("hour");
            }
        }
        if (hu == -1 || hl == -1) {
            return 0;
        }
        int diff = hl - hu;
        if (diff > 12) {
            diff = diff - 24;
        }
        if (diff < -12) {
            diff = diff + 24;
        }
        return diff;
    }

    //return SUNDAY like
    public static String getDayOfWeek(long epoch, int timeZoneDiff) {
        return Instant.ofEpochMilli(epoch).atZone(ZoneId.of(timeZoneDiff < 0 ? String.valueOf(timeZoneDiff) : "+" + timeZoneDiff)).getDayOfWeek().name();
    }

    public static boolean isSunday(long epoch, int timeZoneDiff) {
        return "SUNDAY".equals(getDayOfWeek(epoch, timeZoneDiff));
    }

    public static boolean isSundayEarly(long epoch, int timeZoneDiff) {
        if ("SUNDAY".equals(getDayOfWeek(epoch, timeZoneDiff))) {
            int h = Instant.ofEpochMilli(epoch).atZone(ZoneId.of("UTC")).getHour();
            if (h < 20) {
                return true;
            }
        }
        return false;
    }

    public static boolean isSundayLate(long epoch, int timeZoneDiff) {
        if ("SUNDAY".equals(getDayOfWeek(epoch, timeZoneDiff))) {
            int h = Instant.ofEpochMilli(epoch).atZone(ZoneId.of("UTC")).getHour();
            if (h >= 20) {
                return true;
            }
        }
        return false;
    }

    public static ZonedDateTime getZonedTime(long epoch, int timeZoneDiff) {
        return Instant.ofEpochMilli(epoch).atZone(ZoneId.of(timeZoneDiff < 0 ? String.valueOf(timeZoneDiff) : "+" + timeZoneDiff));
    }

    public static String formatUTC(long epoch) {
        return Instant.ofEpochMilli(epoch).atZone(ZoneId.of("Z")).format(DateTimeFormatter.ISO_DATE_TIME).substring(0, 19);
    }

    public static long parseUTC(String timeString) {
        return Instant.parse(timeString.length() == 19 ? timeString + "Z" : timeString).toEpochMilli();
    }

    public static long parseUTC(JsonObject timeObj) {
        if (timeObj.getString("dateType").equals("UTC")) {
            return parseUTC(timeObj.getString("full"));
        } else {
            return 0;
        }
    }

    public static long parseUTC(JsonArray timeArray) {
        long t = 0;
        for (int i = 0; i < timeArray.size(); i++) {
            t = parseUTC(timeArray.getJsonObject(i));
            if (t > 0) {
                break;
            }
        }
        return t;
    }

    public static ZonedDateTime getPSTTime(long epoch) {
        return Instant.ofEpochMilli(epoch).atZone(ZoneId.of("PST", ZoneId.SHORT_IDS));
    }

    public static long getPSTMilli(long epoch, int hour, int minute, int second) {
        return getPSTTime(epoch).withNano(0).withSecond(second).withMinute(minute).withHour(hour).toEpochSecond() * 1000L;
    }

    //yyyy-MM-dd
    public static String getPSTDate(long epoch) {
        return getPSTTime(epoch).format(DateTimeFormatter.ISO_LOCAL_DATE);
    }

    public static long parsePSTDate(String date) {
        DateTimeFormatter fmt = new DateTimeFormatterBuilder()
                .appendPattern("yyyy-MM-dd")
                .parseDefaulting(ChronoField.MONTH_OF_YEAR, 1)
                .parseDefaulting(ChronoField.DAY_OF_MONTH, 1)
                .parseDefaulting(ChronoField.HOUR_OF_DAY, 0)
                .toFormatter()
                .withZone(ZoneId.of("PST", ZoneId.SHORT_IDS));

        ZonedDateTime zonedDateTime = ZonedDateTime.parse(date, fmt);
        return zonedDateTime.toEpochSecond() * 1000L;
    }

    //yyyy-MM-ddTHH:mm:ss
    public static String formatPST(long epoch) {
        return getPSTTime(epoch).format(DateTimeFormatter.ISO_LOCAL_DATE_TIME).substring(0, 19);
    }

    public static long parseCTT(String timeString) {
        char delimeter = timeString.charAt(4);
        DateTimeFormatter fmt = new DateTimeFormatterBuilder()
                .appendPattern("yyyy" + delimeter + "MM" + delimeter + "dd HH:mm:ss")
                .parseDefaulting(ChronoField.MONTH_OF_YEAR, 1)
                .parseDefaulting(ChronoField.DAY_OF_MONTH, 1)
                .parseDefaulting(ChronoField.HOUR_OF_DAY, 0)
                .toFormatter()
                .withZone(ZoneId.of("CTT", ZoneId.SHORT_IDS));
        return ZonedDateTime.parse(timeString, fmt).toEpochSecond() * 1000L;
    }

    //yyyy-MM-ddTHH:mm:ss
    public static String formatCTT(long epoch) {
        DateTimeFormatter fmt = new DateTimeFormatterBuilder()
                .appendPattern("yyyy-MM-dd HH:mm:ss")
                .parseDefaulting(ChronoField.MONTH_OF_YEAR, 1)
                .parseDefaulting(ChronoField.DAY_OF_MONTH, 1)
                .parseDefaulting(ChronoField.HOUR_OF_DAY, 0)
                .toFormatter()
                .withZone(ZoneId.of("CTT", ZoneId.SHORT_IDS));

        return getCTTTime(epoch).format(fmt);
    }

    public static ZonedDateTime getCTTTime(long epoch) {
        return Instant.ofEpochMilli(epoch).atZone(ZoneId.of("CTT", ZoneId.SHORT_IDS));
    }
}
