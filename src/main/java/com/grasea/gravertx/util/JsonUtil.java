/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.grasea.gravertx.util;

import com.grasea.gravertx.util.TimeUtil;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import java.math.BigDecimal;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 *
 * @author USER
 */
public class JsonUtil {

    public static void putToArrayAttribute(JsonObject obj, String arrayAttribute, Object value) {
        if (!obj.containsKey(arrayAttribute)) {
            obj.put(arrayAttribute, new JsonArray());
        }
        obj.getJsonArray(arrayAttribute).add(value);
    }

    public static void putToObjectAttribute(JsonObject obj, String objectAttribute, String key, Object value) {
        if (!obj.containsKey(objectAttribute)) {
            obj.put(objectAttribute, new JsonObject());
        }
        obj.getJsonObject(objectAttribute).put(key, value);
    }

    public static void pushQueue(JsonObject obj, String arrayAttribute, Object value, int maxSize) {
        if (!obj.containsKey(arrayAttribute)) {
            obj.put(arrayAttribute, new JsonArray());
        }
        obj.getJsonArray(arrayAttribute).add(value);
        while (obj.getJsonArray(arrayAttribute).size() > maxSize) {
            obj.getJsonArray(arrayAttribute).remove(0);
        }
    }

    public static int sumOf(JsonObject obj, String arrayAttribute, String intAttribute) {
        if (obj.containsKey(arrayAttribute)) {
            return sumOf(obj.getJsonArray(arrayAttribute), intAttribute);
        } else {
            return 0;
        }
    }

    public static int sumOf(JsonArray array, String intAttribute) {
        int sum = 0;
        for (int i = 0; i < array.size(); i++) {
            sum += array.getJsonObject(i).getInteger(intAttribute, 0);
        }
        return sum;
    }

    public static JsonObject findObjectInArray(JsonArray array, String attribute, String value) {
        for (int i = 0; i < array.size(); i++) {
            if (array.getJsonObject(i).containsKey(attribute) && value.equals(array.getJsonObject(i).getString(attribute))) {
                return array.getJsonObject(i);
            }
        }
        return null;
    }

    public static String getObjectAttributeStringValue(JsonObject obj, String objectAttribute, String strAttribute) {
        if (obj.containsKey(objectAttribute)) {
            return obj.getJsonObject(objectAttribute).getString(strAttribute, null);
        } else {
            return null;
        }
    }

    public static int getObjectAttributeIntValue(JsonObject obj, String objectAttribute, String intAttribute) {
        if (obj.containsKey(objectAttribute)) {
            return obj.getJsonObject(objectAttribute).getInteger(intAttribute, 0);
        } else {
            return 0;
        }
    }

    public static double getObjectAttributeDoubleValue(JsonObject obj, String objectAttribute, String doubleAttribute) {
        if (obj.containsKey(objectAttribute)) {
            return obj.getJsonObject(objectAttribute).getDouble(doubleAttribute, 0.0);
        } else {
            return 0;
        }
    }

    public static JsonArray getObjectAttributeArrayValue(JsonObject obj, String objectAttribute, String arrayAttribute) {
        if (obj.containsKey(objectAttribute)) {
            return obj.getJsonObject(objectAttribute).getJsonArray(arrayAttribute, new JsonArray());
        } else {
            return new JsonArray();
        }
    }

    public static JsonObject object(JsonObject obj, String objectAttribute) {
        if (obj.containsKey(objectAttribute)) {
            return obj.getJsonObject(objectAttribute);
        } else {
            JsonObject child = new JsonObject();
            obj.put(objectAttribute, child);
            return child;
        }
    }

    public static JsonArray array(JsonObject obj, String arrayAttribute) {
        if (obj.containsKey(arrayAttribute)) {
            return obj.getJsonArray(arrayAttribute);
        } else {
            JsonArray array = new JsonArray();
            obj.put(arrayAttribute, array);
            return array;
        }
    }

    public static ArrayList<JsonObject> toList(JsonArray array) {
        ArrayList<JsonObject> list = new ArrayList<>();
        for (int i = 0; i < array.size(); i++) {
            list.add(array.getJsonObject(i));
        }
        return list;
    }

    public static <T> JsonArray toArray(List<T> list) {
        JsonArray array = new JsonArray();
        list.forEach(jo -> {
            array.add(jo);
        });
        return array;
    }

    public static JsonArray sortArray(JsonArray array, Comparator<JsonObject> comparator) {
        ArrayList<JsonObject> list = toList(array);
        Collections.sort(list, comparator);
        return toArray(list);
    }

    public static <T> T randomPop(List<T> list) {
        if (list == null || list.isEmpty()) {
            return null;
        } else if (list.size() == 1) {
            return list.remove(0);
        } else {
            int index = new SecureRandom().nextInt(list.size());
            return list.remove(index);
        }
    }

    public static boolean in(int value, JsonArray array) {
        for (int i = 0; i < array.size(); i++) {
            if (value == array.getInteger(i)) {
                return true;
            }
        }
        return false;
    }

    public static boolean in(String value, JsonArray array) {
        for (int i = 0; i < array.size(); i++) {
            if (value.equals(array.getString(i))) {
                return true;
            }
        }
        return false;
    }

    //digit為小數點以下幾位的意思，小數點以下1位則傳入1
    public static double round(String number, int digit) {
        return new BigDecimal(number).setScale(digit, BigDecimal.ROUND_HALF_UP).doubleValue();
    }

    public static double round(double number, int digit) {
        return new BigDecimal(number).setScale(digit, BigDecimal.ROUND_HALF_UP).doubleValue();
    }

    public static void markLastUpdateTime(JsonObject jo) {
        jo.put("lut", TimeUtil.getNow()).put("luts", TimeUtil.formatPST(TimeUtil.getNow()));
    }

    public static void markCreateTime(JsonObject jo) {
        jo.put("ct", TimeUtil.getNow()).put("cts", TimeUtil.formatPST(TimeUtil.getNow()));
    }
}
