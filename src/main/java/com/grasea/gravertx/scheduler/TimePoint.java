/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.grasea.gravertx.scheduler;

import java.time.Instant;

/**
 *
 * @author USER
 */
public class TimePoint {

    protected Period period;
    protected String timeZoneId;
    protected boolean executeImmediately;
    protected int baseDayOfWeek = -1;
    protected int baseHour = -1;
    protected int baseMinute = -1;
    protected long startMilli = 0;
    protected long delay = 0;

    private TimePoint(String timeZone) {
        if (timeZone.equals("PST")) {
            timeZoneId = "America/Los_Angeles";
        } else {
            timeZoneId = timeZone;
        }
    }

    public static TimePoint create() {
        return new TimePoint("UTC");
    }

    public static TimePoint create(String timeZoneID) {
        return new TimePoint(timeZoneID);
    }

    public TimePoint period(Period p) {
        this.period = p;
        return this;
    }

    public TimePoint immediately() {
        executeImmediately = true;
        return this;
    }

    public TimePoint delay(Long delayMilliSecond) {
        delay = delayMilliSecond;
        return this;
    }

//    public TimePoint atDate(int date) {
//        this.baseDate = date;
//        return this;
//    }
//
//    public TimePoint atDateTime(int date, int hour, int minute) {
//        this.baseDate = date;
//        this.baseHour = hour;
//        this.baseMinute = minute;
//        return this;
//    }
    //dayOfWeek plz refer org.joda.time.DateTimeConstants, monday = 1, sunday = 7
    public TimePoint atDayOfWeekTime(int dayOfWeek, int hour, int minute) {
        this.period = Period.Week;
        baseDayOfWeek = dayOfWeek;
        this.baseHour = hour;
        this.baseMinute = minute;
        return this;
    }

    public TimePoint atDailyTime(int hour, int minute) {
        this.period = Period.Day;
        this.baseHour = hour;
        this.baseMinute = minute;
        return this;
    }

    public TimePoint atFixedMinute(int minute) {
        this.period = Period.Hour;
        this.baseMinute = minute;
        return this;
    }

    public TimePoint fromTime(int hour, int minute) {
        this.baseHour = hour;
        this.baseMinute = minute;
        return this;
    }

    protected long calculateStartMilli() {
        //ignore the executeImmediately variable, just considering baseDate, baseHour, baseMinute and baseSecond and period
        return period.getNextTime(baseDayOfWeek, baseHour, baseMinute, timeZoneId);
    }

    public long getStartMilli() {
        if (startMilli <= 0) {
            startMilli = calculateStartMilli();
        }
        long now = Instant.now().toEpochMilli();
//        if (startMilli < now) {
//            return 0;
//        }
        while (startMilli < now) {
            startMilli += period.getInterval();
        }
        if (executeImmediately) {
            executeImmediately = false;
            if (startMilli - now <= Period.FiveSecond.getInterval()) {
                return Math.max(startMilli + delay, now + 500);
            } else {
                return now + Math.max(500, delay);
            }
        } else {
            return startMilli + delay;
        }
    }

    public long getInterval() {
        return period.getInterval();
    }

    public void next() {
        while (startMilli <= Instant.now().toEpochMilli()) {
            startMilli += period.getInterval();
        }
    }
}
