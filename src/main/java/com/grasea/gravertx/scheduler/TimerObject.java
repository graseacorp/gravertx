/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.grasea.gravertx.scheduler;

import io.vertx.core.json.JsonObject;

/**
 *
 * @author USER
 */
public class TimerObject {

    protected TimerizeCollection collection;
    protected JsonObject data;
    protected long firedTime;
    protected long taskId;

    public TimerObject(TimerizeCollection collection, JsonObject data, long firedTime, long taskId) {
        this.collection = collection;
        this.data = data;
        this.firedTime = firedTime;
        this.taskId = taskId;
    }

    public TimerizeCollection getCollection() {
        return collection;
    }

    public void setCollection(TimerizeCollection collection) {
        this.collection = collection;
    }

    public JsonObject getData() {
        return data;
    }

    public void setData(JsonObject data) {
        this.data = data;
    }

    public long getFiredTime() {
        return firedTime;
    }

    public void setFiredTime(long firedTime) {
        this.firedTime = firedTime;
    }

    public long getTaskId() {
        return taskId;
    }

    public void setTaskId(long taskId) {
        this.taskId = taskId;
    }

}
