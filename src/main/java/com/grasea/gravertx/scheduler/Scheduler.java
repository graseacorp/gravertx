/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.grasea.gravertx.scheduler;

import com.grasea.gravertx.module.TimerManager;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import java.time.Instant;
import java.util.concurrent.ConcurrentHashMap;

/**
 *
 * @author USER
 */
public class Scheduler {

    private static ConcurrentHashMap<String, Long> timerMap = new ConcurrentHashMap<>();

    public static Long startAt(Vertx vertx, String taskKey, long startMilli, Handler<Long> h) {
        long taskId = vertx.setTimer(startMilli - Instant.now().toEpochMilli(), l -> {
            TimerManager.get().done(taskKey);
            h.handle(l);
        });
        return taskId;
    }

    public static void startPeriod(Vertx vertx, long startMilli, long interval, Handler<Long> h) {
        vertx.setTimer(startMilli - Instant.now().toEpochMilli(), l -> {
            h.handle(l);
            //System.out.println("schedule next task at " + ((startMilli + interval) - Instant.now().toEpochMilli()) + "ms later");
            if (interval > 0) {
                startPeriod(vertx, startMilli + interval, interval, h);
            }
        });
    }

    public static void startPeriod(Vertx vertx, TimePoint timePoint, Handler<Long> h) {
        vertx.setTimer(timePoint.getStartMilli() - Instant.now().toEpochMilli(), l -> {
            h.handle(l);
            //System.out.println("schedule next task at " + ((startMilli + interval) - Instant.now().toEpochMilli()) + "ms later");
            timePoint.next();
            startPeriod(vertx, timePoint, h);
        });
    }
}
