/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.grasea.gravertx.scheduler;

import com.grasea.gravertx.util.TimeUtil;
import io.vertx.core.json.JsonObject;

/**
 *
 * @author USER
 */
public class TimerCondition {

    protected long from = -1;
    protected long to = -1;
    protected JsonObject precondition;
    protected TimerSetupPolicy policy;

    public TimerCondition(TimerSetupPolicy policy) {
        precondition = null;
        this.policy = policy;
        from = TimeUtil.getNow();//should get from TimerSetupPolicy
        to = -1;//should get from TimerSetupPolicy
    }

    public TimerCondition(JsonObject precondition) {
        this.precondition = precondition;
        this.policy = TimerSetupPolicy.All;
        from = TimeUtil.getNow();
        to = -1;
    }

    public TimerCondition(TimerSetupPolicy policy, JsonObject precondition) {
        this.precondition = precondition;
        this.policy = policy;
    }

    public TimerCondition from(long from) {
        this.from = from;
        return this;
    }

    public TimerCondition to(long to) {
        this.to = to;
        return this;
    }

    public JsonObject getCondition(String timeField) {
        JsonObject cond = new JsonObject();

        if (from > 0) {
            cond.put("$gte", from);
        }
        if (to > 0) {
            cond.put("$lt", to);
        }
        if (!cond.fieldNames().isEmpty()) {
            cond = new JsonObject().put(timeField, cond);
        }
        if (precondition != null) {
            cond.mergeIn(precondition);
        }
        return cond;
    }

    public JsonObject getPrecondition() {
        return precondition;
    }

    public void setPrecondition(JsonObject precondition) {
        this.precondition = precondition;
    }

    public TimerSetupPolicy getPolicy() {
        return policy;
    }
}
