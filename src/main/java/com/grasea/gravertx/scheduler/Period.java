/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.grasea.gravertx.scheduler;

import com.grasea.gravertx.util.TimeUtil;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.joda.time.MutableDateTime;

/**
 *
 * @author USER
 */
public enum Period {
    Second(1000L), FiveSecond(5000L), QuarterMinute(15000L), HalfMinute(30000L), Minute(60000L), FiveMinutes(300000L), QuarterHour(900000L), HalfHour(1800000L), Hour(3600000L), TwoHour(7200000L), ThreeHour(10800000L), SixHour(21600000L), EightHour(28800000L), Day(24 * 3600000L), Week(7 * 24 * 3600000L);

    long interval;

    Period(long interval) {
        this.interval = interval;
    }

    public long getInterval() {
        return interval;
    }

    public long getNextTime(int dayOfWeek, int hour, int minute) {
        return getNextTime(dayOfWeek, hour, minute, "UTC");
    }

    public long getNextTime(int dayOfWeek, int hour, int minute, String timeZone) {
        //Date dt=new Date();
        MutableDateTime dt = new MutableDateTime();
        dt.setZone(DateTimeZone.forID(timeZone));
        switch (this) {
            case Week:
                if (hour > -1) {
                    dt.setHourOfDay(hour);
                }
                if (minute > -1) {
                    dt.setMinuteOfHour(minute);
                    dt.setSecondOfMinute(0);
                }
                dt.setMillisOfSecond(0);
                if (dt.getDayOfWeek() != dayOfWeek) {
                    int diffDays = dayOfWeek - dt.getDayOfWeek();
                    if (diffDays < 0) {
                        diffDays += 7;
                    }
                    dt.addDays(diffDays);
                }
                while (dt.isBeforeNow()) {
                    dt.addDays(7);
                }
                break;
            case Day:
                if (hour > -1) {
                    dt.setHourOfDay(hour);
                    dt.setMillisOfSecond(0);
                }
                if (minute > -1) {
                    dt.setMinuteOfHour(minute);
                    dt.setSecondOfMinute(0);
                }
                while (dt.isBeforeNow()) {
                    dt.addDays(1);
                }
                break;
            default:

                long ms = dt.getMillisOfDay();
                if (hour > 0) {
                    ms -= hour * 3600000L;
                    if (ms < 0) {
                        ms += TimeUtil.getDaysMilliSecond(1);
                    }
                }
                long needPlus = interval - ms % interval + (minute * 60000L);
                while (needPlus > interval) {
                    needPlus -= interval;
                }
                if (needPlus < 1000) {
                    needPlus += interval;
                }
                dt.add(needPlus);
        }
        return dt.getMillis();
    }
}
