/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.grasea.gravertx.scheduler;

import com.grasea.gravertx.module.TimerManager;
import io.vertx.core.Handler;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.mongo.MongoClient;
import java.util.concurrent.ConcurrentHashMap;

/**
 *
 * @author USER
 */
public class TimerizeCollection {

    protected String jobName;
    protected String collection;
    protected String timeField;
    protected String keyField;
    protected TimerCondition condition;
    protected Handler<JsonObject> handler;

    public TimerizeCollection(String jobName, String collection, String keyField, String timeField, TimerCondition condition, Handler<JsonObject> handler) {
        this.jobName = jobName;
        this.collection = collection;
        this.keyField = keyField;
        this.timeField = timeField;
        this.condition = condition;
        this.handler = handler;
    }

    public String getObjectKey(JsonObject obj) {
        return obj.getString(keyField) + "-" + timeField;
    }

    public void initializeMatchedObjects(MongoClient client, ConcurrentHashMap<String, TimerObject> timerObjectMap) {
        client.find(collection, condition.getCondition(timeField), res -> {
            if (res.succeeded()) {
                res.result().forEach(obj -> {
                    String key = getObjectKey(obj);
                    long t = obj.getLong(timeField);
                    Long taskId = Scheduler.startAt(TimerManager.get().getServer().getVertx(), key, t, l -> {
                        handler.handle(obj);
                    });
                    TimerObject to = new TimerObject(this, obj, t, taskId);
                    timerObjectMap.put(key, to);
                });
            }
        });
    }

    public void setupObject(JsonObject obj, ConcurrentHashMap<String, TimerObject> timerObjectMap) {
        String key = getObjectKey(obj);
        long t = obj.getLong(timeField);
        Long taskId = Scheduler.startAt(TimerManager.get().getServer().getVertx(), key, t, l -> {
            handler.handle(obj);
        });
        TimerObject to = new TimerObject(this, obj, t, taskId);
        timerObjectMap.put(key, to);
    }

    public String getJobName() {
        return jobName;
    }

    public String getCollection() {
        return collection;
    }

    public String getTimeField() {
        return timeField;
    }

    public String getKeyField() {
        return keyField;
    }

    public TimerCondition getCondition() {
        return condition;
    }

    public Handler<JsonObject> getHandler() {
        return handler;
    }

}
