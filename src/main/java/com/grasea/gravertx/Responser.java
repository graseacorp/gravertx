/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.grasea.gravertx;

import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.FileUpload;
import io.vertx.ext.web.RoutingContext;
import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author USER
 */
public class Responser {

    protected final static String MESSAGE_GENERAL_ERROR = "System Error";
    protected final static String RESULT = "result";
    protected final static int CODE_OK = 1;
    protected final static int CODE_NG = 0;
    protected String siteDomain;

    public Responser(String siteDomain) {
        this.siteDomain = siteDomain;
    }

    public Responser() {
    }

    public void setSiteDomain(String siteDomain) {
        this.siteDomain = siteDomain;
    }

    public HttpServerResponse fillHeader(RoutingContext context) {
        return fillHeader(context, "application/json");
    }

    public HttpServerResponse fillHeader(RoutingContext context, String contentType) {
        return fillHeader(context, null, contentType);
    }

    public HttpServerResponse fillHeader(RoutingContext context, String contentDisposition, String contentType) {
        HttpServerResponse res = context.response();
        if (contentDisposition != null) {
            res.putHeader("Content-Disposition", contentDisposition);
        }
        if (contentType != null) {
            res.putHeader("Content-Type", contentType);
        }
        return res;
    }

    public HttpServerResponse fillHeaderWithCrossDomain(RoutingContext context) {
        return fillHeaderWithCrossDomain(context.response().putHeader("content-type", "application/json"));
    }

    public HttpServerResponse fillHeaderWithCrossDomain(HttpServerResponse res) {
        return res.putHeader("Access-Control-Allow-Origin", siteDomain == null ? "http://localhost:6780" : siteDomain)
                .putHeader("Access-Control-Allow-Methods", "GET, POST, OPTIONS")
                .putHeader("Access-Control-Allow-Headers", "X-Requested-With, Content-Type, Accept, Set-Cookie")
                .putHeader("Access-Control-Allow-Credentials", "true");
    }

    public void download(RoutingContext context, String fileName, String text) {
        HttpServerResponse res = fillHeader(context, "attachment; filename=" + fileName, "text/text");
        fillHeaderWithCrossDomain(res).end(text);
    }

    public void text(RoutingContext context, String text) {
        HttpServerResponse res = fillHeader(context, "text/text");
        fillHeaderWithCrossDomain(res).end(text);
    }

    public void file(GravertxServer server, RoutingContext context, String path) {
        if (server.getVertx().fileSystem().existsBlocking(path)) {
            context.response().sendFile(path);
        } else {
            //ng(context, new JsonObject().put("message", "file " + path + " doesn't exist"));
            ng(context, 404, "File " + path + " doesn't exist.");
        }
    }

    public void ok(RoutingContext context) {

        fillHeaderWithCrossDomain(context).end(jsonp(context, new JsonObject().put(RESULT, CODE_OK)));
    }

    public void ok(RoutingContext context, String message) {
        fillHeaderWithCrossDomain(context).end(jsonp(context, new JsonObject().put(RESULT, CODE_OK).put("message", message)));
    }

    public void ok(RoutingContext context, JsonObject json) {
        fillHeaderWithCrossDomain(context).end(jsonp(context, json.put(RESULT, CODE_OK)));
    }

    public void ng(RoutingContext context) {
        ng(context, new JsonObject());
    }

    public void ng(RoutingContext context, int errorCode) {
        context.response().setStatusCode(errorCode).end();
    }

    public void ng(RoutingContext context, int errorCode, String message) {
        context.response().setStatusCode(errorCode).setStatusMessage(message).end();
    }

    public void ng(RoutingContext context, String message, Throwable t) {
        ng(context, new JsonObject().put("message", message).put("reason", t.getMessage()));
    }

    public void ng(RoutingContext context, Throwable t) {
        ng(context, new JsonObject().put("message", MESSAGE_GENERAL_ERROR).put("reason", t.getMessage()));
    }

    public void ng(RoutingContext context, String message) {
        ng(context, new JsonObject().put("message", message));
    }

    public void ng(RoutingContext context, JsonObject json) {
        fillHeaderWithCrossDomain(context).end(jsonp(context, json.put(RESULT, CODE_NG)));
        if (!context.fileUploads().isEmpty()) {
            for (FileUpload file : context.fileUploads()) {
                context.vertx().fileSystem().delete(file.uploadedFileName(), res -> {
                    if (res.failed()) {
                        Logger.getLogger(Responser.class.getName()).log(Level.SEVERE, "cannot delete upload file: " + file.uploadedFileName());
                    } else {
                        System.out.println("deleted upload file: " + file.uploadedFileName());
                    }
                });
            }
        }
    }

    protected String jsonp(RoutingContext context, JsonObject json) {
        if (context.request().getParam("callback") != null) {
            return context.request().getParam("callback") + "(" + json.encode() + ");";
        } else {
            return json.encode();
        }
    }

    public void redirect(RoutingContext context, String redirectURL) {
        context.response().setStatusCode(301).putHeader("Location", redirectURL).end();
    }
}
