/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.grasea.gravertx;

/**
 *
 * @author Rovers <rovers@grasea.com>
 */
public enum SQLProfile {
    PostgreSQL("\"", "\""), MSSQL("[", "]"), MYSQL("`", "`");

    String quote1;
    String quote2;

    SQLProfile(String quote1, String quote2) {
        this.quote1 = quote1;
        this.quote2 = quote2;
    }
}
