/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.grasea.gravertx.helper;

import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.templ.HandlebarsTemplateEngine;

/**
 *
 * @author USER
 */
public class TemplateHelper {

    protected static final HandlebarsTemplateEngine engine = HandlebarsTemplateEngine.create();

    public static void render(RoutingContext rc) {
        rc.response().putHeader("Cache-Control", "no-cache, no-store, must-revalidate");
        for (String key : rc.session().data().keySet()) {
            rc.put(key, rc.session().get(key).toString());
        }
        engine.render(rc, "webroot" + rc.request().path() + ".hbs", res -> {//
            if (res.succeeded()) {
                rc.response().end(res.result());
            } else {
                rc.fail(res.cause());
            }
        });
    }

    public static void renderDenyCache(RoutingContext ctx) {
        ctx.response().putHeader("Cache-Control", "no-cache, no-store, must-revalidate");
        for (String key : ctx.session().data().keySet()) {
            ctx.put(key, ctx.session().get(key).toString());
        }
        engine.render(ctx, "webroot" + ctx.request().path() + ".hbs", res -> {//
            if (res.succeeded()) {
                ctx.response().end(res.result());
            } else {
                ctx.fail(res.cause());
            }
        });
    }
}
