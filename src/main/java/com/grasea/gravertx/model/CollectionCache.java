/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.grasea.gravertx.model;

import com.grasea.gravertx.util.TimeUtil;
import io.vertx.core.AsyncResult;
import io.vertx.core.Future;
import io.vertx.core.Handler;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.mongo.MongoClient;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.concurrent.ConcurrentHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author USER
 */
public class CollectionCache {

    protected String collection;
    protected MongoClient mongoClient;
    protected ConcurrentHashMap<String, JsonObject> cacheData;
    protected String uniqueField;

    public CollectionCache(MongoClient mongoClient, String collection, String uniqueField) {
        this.collection = collection;
        this.mongoClient = mongoClient;
        this.uniqueField = uniqueField;
        cacheData = new ConcurrentHashMap<>();
    }

    public void loadCollectionData() {
        loadCollectionData(res -> {
            if (res.failed()) {
                Logger.getLogger(CollectionCache.class.getName()).log(Level.SEVERE, null, res.cause());
            }
        });
    }

    public void loadCollectionData(Handler<AsyncResult<Boolean>> h) {
        loadCollectionData(collection, cacheData, h);
    }

    protected void loadCollectionData(String col, ConcurrentHashMap<String, JsonObject> cacheData, Handler<AsyncResult<Boolean>> h) {
        cacheData.clear();
        mongoClient.find(col, new JsonObject(), res -> {
            if (res.succeeded()) {
                res.result().forEach(obj -> {
                    if (obj.getString(uniqueField) == null) {
                        //Logger.getLogger(CollectionCache.class.getName()).log(Level.SEVERE, "load data from db error: [" + collection + "] " + obj);
                        obj.put(uniqueField, obj.getString("_id"));
                    }
                    cacheData.put(obj.getString(uniqueField), obj);
                });
                h.handle(Future.succeededFuture(true));
            } else {
                h.handle(Future.failedFuture(res.cause()));
            }
        });
    }

    public void insertIfNotExist(JsonObject jo, Handler<AsyncResult<Boolean>> h) {
        JsonObject condition = new JsonObject();
        if (jo.containsKey(uniqueField)) {
            condition.put(uniqueField, jo.getValue(uniqueField));
        } else if (jo.containsKey("_id")) {
            condition.put("_id", jo.getString("_id"));
        } else {
            h.handle(Future.failedFuture("obj without key and _id cannot be checked"));
        }
        mongoClient.findOne(collection, condition, new JsonObject(), res -> {
            if (res.succeeded()) {
                if (res.result() == null) {
                    update(jo, h);
                } else {
                    h.handle(Future.failedFuture("mongodb exist obj: " + condition));
                    //updateDataMap(collectionKey, jo, f);
                }
            } else {
                h.handle(Future.failedFuture(res.cause()));
            }
        });
    }

    public void update(JsonObject jo, Handler<AsyncResult<Boolean>> h) {
        //System.out.println("update " + collection + " object, key=" + jo.getString(uniqueField));
        jo.put("lut", TimeUtil.getNow()).put("luts", TimeUtil.formatUTC(TimeUtil.getNow()));
        if (jo.containsKey(uniqueField)) {
            if (cacheData.containsKey(jo.getString(uniqueField))) {
                final JsonObject origin = cacheData.get(jo.getString(uniqueField));
                if (origin == null) {
                    cacheData.remove(jo.getString(uniqueField));
                    //System.out.println("remove obj key=" + jo.getString(uniqueField) + " from collection " + collection);
                    h.handle(Future.succeededFuture(false));
                    LoggerFactory.getLogger(CollectionCache.class).error("Error: null obj in map " + collection + " for key " + jo.getString(uniqueField));
                } else {
                    if (origin != jo) {
                        origin.mergeIn(jo);
                    }
                    if (origin.containsKey("_id")) {
                        mongoClient.save(collection, origin, res -> {
                            if (res.succeeded()) {
                                //System.out.println("updated obj " + origin);
                                h.handle(Future.succeededFuture(true));
                            } else {
                                LoggerFactory.getLogger(CollectionCache.class).error("Cannot store data to collection: " + collection);
                                h.handle(Future.succeededFuture(false));
                            }
                        });
                        //String id = awaitResult(h -> mongoClient.save(collectionKey, origin, h));
                    } else {
                        mongoClient.insert(collection, origin, res -> {
                            if (res.succeeded()) {
                                String id = res.result();
                                origin.put("_id", id);
                                //System.out.println("exist but no _id, insert obj " + id + ": " + origin);
                                h.handle(Future.succeededFuture(true));
                            } else {
                                LoggerFactory.getLogger(CollectionCache.class).error("Cannot store data to collection: " + collection);
                                h.handle(Future.succeededFuture(false));
                            }
                        });
                        //String id = awaitResult(h -> mongoClient.insert(collectionKey, origin, h));
                        //origin.put("_id", id);
                    }
                }
            } else {
                cacheData.put(jo.getString(uniqueField), jo);
                mongoClient.insert(collection, jo, res -> {
                    if (res.succeeded()) {
                        String id = res.result();
                        jo.put("_id", id);
                        //System.out.println("not in map, insert obj " + jo.toString());
                        h.handle(Future.succeededFuture(true));
                    } else {
                        LoggerFactory.getLogger(CollectionCache.class).error("Cannot store data to collection: " + collection);
                        h.handle(Future.succeededFuture(false));
                    }
                });
                //String id = awaitResult(h -> mongoClient.insert(collectionKey, jo, h));
                //jo.put("_id", id);
                //map.put(jo.getString(uniqueField), jo);
            }
        } else {
            mongoClient.insert(collection, jo, res -> {
                if (res.succeeded()) {
                    String id = res.result();
                    jo.put("_id", id);
                    jo.put(uniqueField, id);
                    cacheData.put(jo.getString(uniqueField), jo);
                    //System.out.println("insert obj " + jo.toString());
                    mongoClient.save(collection, jo, res2 -> {
                        if (res2.succeeded()) {
                            h.handle(Future.succeededFuture(true));
                        } else {
                            h.handle(Future.failedFuture(new Exception("update db fail")));
                        }
                    });
                } else {
                    h.handle(Future.succeededFuture(false));
                }
            });
            //String id = awaitResult(h -> mongoClient.insert(collectionKey, jo, h));
            //jo.put("_id", id);
            //jo.put(uniqueField, id);
            //map.put(id, jo);
        }
        //System.out.println("updated obj");
    }

    public boolean containsKey(String key) {
        return cacheData.containsKey(key);
    }

    public JsonObject get(String key) {
        return cacheData.get(key);
    }

    public void iterate(Handler<JsonObject> handler) {
        cacheData.values().forEach(obj -> handler.handle(obj));
    }

    public ArrayList<String> keys() {
        ArrayList<String> keys = new ArrayList<>();
        Enumeration<String> e = cacheData.keys();
        while (e.hasMoreElements()) {
            keys.add(e.nextElement());
        }
        return keys;
    }

    public ArrayList<JsonObject> values() {
        return new ArrayList<>(cacheData.values());
    }

    public void clear() {
        cacheData.clear();
    }
}
