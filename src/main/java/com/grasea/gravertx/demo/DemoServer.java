/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.grasea.gravertx.demo;

import com.grasea.gravertx.GravertxServer;
import com.grasea.gravertx.Table;
import com.grasea.gravertx.demo.module.BookManager;
import com.grasea.gravertx.module.AccountManager;
import com.grasea.gravertx.module.MongoManager;
import io.vertx.core.AsyncResult;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Router;
import java.util.ArrayList;
import java.util.List;
import static io.vertx.ext.sync.Sync.awaitResult;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Grasea
 */
public class DemoServer extends GravertxServer {

    @Override
    protected void init(Router router) {
        System.out.println("start init " + this.getClass().getSimpleName());
        setupPostgreSQL("localhost", 5432, "FantasySpin", "postgres", "690918");
        setupMongoDB("BookStore");

        //initDatabase();
        //initMongoDB();
        enableJDBCAuthentication(router);

        //AuthHandler authHandler = BasicAuthHandler.create(authProvider);
        new AccountManager(this).registerRouter(router);
        new BookManager(this).registerRouter(router);
        new MongoManager(this).registerRouter(router);
    }

    @Override
    public void log(AsyncResult res) {
        if (res.failed()) {
            Logger.getLogger(DemoServer.class.getName()).log(Level.SEVERE, null, res.cause());
        }
    }

    protected void initMongoDB() {
        JsonObject document = new JsonObject().put("title", "The Hobbit");
        mongoClient.save("Book", document, res -> {
            if (res.succeeded()) {
                String id = res.result();
                System.out.println("Saved book with id " + id);
            } else {
                res.cause().printStackTrace();
            }
        });
    }

    protected void initDatabase() {
        jdbcClient.getConnection(conn -> {
            if (conn.succeeded()) {
                ArrayList<String> sqls = new ArrayList<>();
                sqls.add("CREATE EXTENSION IF NOT EXISTS citext;");
                sqls.add("CREATE EXTENSION IF NOT EXISTS pgcrypto;");
                sqls.add("drop table if exists " + Table.ACCOUNT + ";");
                sqls.add("drop table if exists " + Table.ACCOUNT_ROLES + ";");
                sqls.add("drop table if exists " + Table.ROLES_PERMS + ";");
                sqls.add("create table " + Table.ACCOUNT + " (id serial PRIMARY KEY, account citext unique, password varchar(255), salt varchar(64) default upper(concat(md5(random()::text),md5(random()::text))));");
                sqls.add("create table " + Table.ACCOUNT_ROLES + " (id serial PRIMARY KEY, account citext, role varchar(255));");
                sqls.add("create table " + Table.ROLES_PERMS + " (id serial PRIMARY KEY, role varchar(255), perm varchar(255));");
                if (!sqls.isEmpty()) {
                    conn.result().batch(sqls, results -> {
                        if (!results.succeeded()) {
                            System.out.println("database initialize fail: " + results.cause().getMessage());
                            System.exit(1);
                        }
                    });
                } else {
                    System.out.println("database initialized");
                }
            }
        });
    }

    public static void main(String[] args) {
        System.out.println("run " + DemoServer.class.getSimpleName());
        GravertxServer.run(DemoServer.class, 8080);
    }
}
