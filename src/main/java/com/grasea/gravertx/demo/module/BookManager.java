/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.grasea.gravertx.demo.module;

import com.grasea.gravertx.GravertxServer;
import com.grasea.gravertx.module.BaseManager;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.sync.Sync;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;

/**
 *
 * @author USER
 */
public class BookManager extends BaseManager {

    public BookManager(GravertxServer server) {
        super(server);
    }

    @Override
    public void registerRouter(Router router) {
        router.get("/books").handler(this::listBooks);
        router.post("/book/add").handler(this::addBook);
        router.get("/book/:title").handler(this::queryBook);
        router.get("/sync_test").handler(Sync.fiberHandler(this::testSync));
    }

    public void testSync(RoutingContext routingContext) {
        //long l1 = awaitEvent(h -> server.getVertx().setTimer(2000, h));
        //long l2 = awaitEvent(h -> server.getVertx().setTimer(2000, h));\
        
        responser.ok(routingContext);
    }

    public void listBooks(RoutingContext routingContext) {
        server.getMongoClient().find("Book", new JsonObject(), result -> {
            if (result.succeeded()) {
                responser.ok(routingContext, new JsonObject().put("books", result.result()));
            } else {
                responser.ng(routingContext, "no book exists", result.cause());
            }
        });
    }

    public void addBook(RoutingContext routingContext) {
        String title = routingContext.request().getParam("title");

        server.getMongoClient().findOne("Book", new JsonObject().put("title", title), null, result -> {
            if (result.succeeded()) {
                if (result.result() != null) {
                    responser.ng(routingContext, "book exists");
                } else {
                    server.getMongoClient().save("Book", new JsonObject().put("title", title), saveResult -> {
                        if (saveResult.succeeded()) {
                            responser.ok(routingContext, "add book success");
                        } else {
                            responser.ng(routingContext, "save book fail", saveResult.cause());
                        }
                    });

                }
            } else {
                responser.ng(routingContext, "query book fail", result.cause());
            }
        });
    }

    public void queryBook(RoutingContext routingContext) {
        String title = routingContext.request().getParam("title");
        server.getMongoClient().findOne("Book", new JsonObject().put("title", title), null, result -> {
            if (result.succeeded()) {
                if (result.result() != null) {
                    responser.ok(routingContext, new JsonObject().put("book", result.result()));
                } else {
                    responser.ng(routingContext, "book not exist");
                }
            } else {
                responser.ng(routingContext, "query book fail", result.cause());
            }
        });
    }
}
